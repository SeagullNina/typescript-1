/** Функции */
function log(message: string): void {
  console.log(message)
}

const arrowLog = (message: string): void => {
  console.log(message)
}

function sum(x: number, y: number): number {
  return x + y
}

function toUpperCase(str: string): string {
  return str.trim().toUpperCase()
}

function getToUpperCase(str: string): ((value: string) => string) {
  return str.toUpperCase
}

/** Асинхронные */
function timeout(delay?: number): Promise<string> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('success')
    }, delay || 300)
  })
}

async function getData(): Promise<void> {
  const data = await timeout(500)
  console.log(data)
}


/** never*/
function error(message: string): never {
  throw new Error(message);
}

function infinite(): never {
  while (true) {
  }
}

function infiniteRec(): never {
  return infiniteRec();
}


function func(): void
function func(a: number): string
function func(a: number, b: number): number

function func(a?: number, b?: number) {
  if (!a && !b) {
    return console.log('undefined args')
  }

  if (a && !b) {
    return a.toString()
  } 

  return a + b
}