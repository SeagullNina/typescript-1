/**typeof, as, instanceof */
function funcExample(value: string | (() => void)) {
  if (typeof value === 'string') {
    return value.toUpperCase()
  }
  return value();
}

class ChosenOne {
  killTheDragon() {
    console.log('dragon was be killed')
  }
}

class Pleb {
  work() {
    console.log('I am working')
  }
}

function handle1(man: ChosenOne | Pleb) {
  (man as ChosenOne).killTheDragon()
}

function handle2(man: ChosenOne | Pleb) {
  if (man instanceof ChosenOne) {
    man.killTheDragon()
  } else {
    man.work();
  }
}

/**keyof */
interface Employee {
  name: string
  age: number
  avatar?: string
  position: string
}

type EmployeeKeys = keyof Employee

let key: EmployeeKeys = 'name'
// key = 'po'

type EmployeeKeysNoAvatar1 = Exclude<keyof Employee, 'avatar'>
type EmployeeKeysNoAvatar2 = keyof Pick<Employee, 'name' | 'age' | 'position'>


let employee1: EmployeeKeysNoAvatar1 = 'name'
// employee1 = 'avatar'

let employee2: EmployeeKeysNoAvatar2 = 'age'
// employee2 = 'avatar'

/**Generic */
type Nullable<T> = null | undefined | T

let lastname: Nullable<string> = null
lastname = 'Ivanov'

let size: Nullable<number> = undefined
size = 140

const arrayOfNumbers: Array<number> = [1, 2, 3, 5]
const arrayOfStrings: Array<string> = ['qwerty', 'asdfg']


function reverse<T>(array: T[]): T[] {
  return array.reverse()
}

reverse(arrayOfNumbers)
reverse(arrayOfStrings)
